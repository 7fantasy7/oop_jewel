package test.ru.botyanov.jewel.action;

import org.junit.Assert;
import org.junit.Test;
import ru.botyanov.jewel.action.NecklaceAction;
import ru.botyanov.jewel.creator.JewelCreator;
import ru.botyanov.jewel.entity.Necklace;
import ru.botyanov.jewel.entity.jewel.Jewel;
import ru.botyanov.jewel.entity.jewel.Opal;
import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;
import ru.botyanov.jewel.exception.JewelIllegalArgumentException;

import java.util.ArrayList;

public class NecklaceActionTest {
    private final static double FROM = 0.42;
    private final static double TO = 0.49;

    @Test
    public void sortTest() {
        ArrayList<Jewel> list = JewelCreator.createListJewels();
        Necklace necklace = new Necklace(list);
        NecklaceAction.sort(necklace);
        for (int i = 0; i < list.size() - 1; i++) {
            Assert.assertTrue(necklace.get(i).getPrice() <= necklace.get(i + 1).getPrice());
        }
    }

    @Test( expected = JewelIllegalArgumentException.class )
    public void testSetPrice() throws JewelIllegalArgumentException {
        Jewel opal = new Opal("Opal", 50, 9, 0.62, Color.BLACK, Cut.TRILLIANT,
                Opal.Type.PERIDOT, Opal.Impurity.Fe2O3, false);
        int expected = 5;
        opal.setPrice(-5);
        Assert.assertEquals("For setPrice(-5) wasn't exception:", expected, opal.getPrice(), 0);
    }

    @Test
    public void testFindJewelsByTransparency() {
        ArrayList<Jewel> list = JewelCreator.createListJewels();
        Necklace necklace = new Necklace(list);
        int expected = 2;
        ArrayList<Jewel> actual = NecklaceAction.findJewelsByTransparency(necklace,FROM,TO);
        Assert.assertEquals(expected, actual.size(), 0);
    }

    @Test
    public void testCalculateWeight() {
        ArrayList<Jewel> list = JewelCreator.createListJewels();
        Necklace necklace = new Necklace(list);
        int actual = NecklaceAction.calculateWeight(necklace);
        int expected = 0;
        for (Jewel jewel : list) {
                expected += jewel.getWeight();
            }
        Assert.assertEquals(expected, actual, 0);
    }

    @Test
    public void testCalculatePrice() {
        ArrayList<Jewel> list = JewelCreator.createListJewels();
        Necklace necklace = new Necklace(list);
        int actual = NecklaceAction.calculatePrice(necklace);
        int expected = 0;
        for (Jewel jewel : list) {
            expected += jewel.getPrice();
        }
        Assert.assertEquals(expected, actual, 0);
    }
}