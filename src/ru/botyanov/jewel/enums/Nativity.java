package ru.botyanov.jewel.enums;

public enum Nativity {
    NATIVE, SYNTHETIC
}
