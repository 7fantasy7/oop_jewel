package ru.botyanov.jewel.enums;

public enum Cut {
    BRILLIANT, CABOCHON, EMERALD, HEART, MOGUL, OBUS, OVAL, ROUND, PRINCESS, TRILLIANT
}