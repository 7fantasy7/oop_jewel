package ru.botyanov.jewel.serial;

import org.apache.log4j.Logger;
import ru.botyanov.jewel.entity.jewel.Jewel;

import java.io.*;
public class Serializator {
    public static final Logger LOG = Logger.getLogger(Serializator.class);

    public boolean serialization(Jewel j, String fileName) {
        boolean flag = false;
        File f = new File(fileName);
        ObjectOutputStream ostream = null;
        try {
            FileOutputStream fos = new FileOutputStream(f);
                ostream = new ObjectOutputStream(fos);
                ostream.writeObject(j);
                flag = true;
        } catch (FileNotFoundException e) {
            LOG.error("File can not be created: " + e);
        } catch (NotSerializableException e) {
            LOG.error("This class is not serializable: " + e);
        } catch (IOException e) {
           LOG.error(e);
        } finally {
            try {
                if (ostream != null) {
                    ostream.close();
                }
            } catch (IOException e) {
                LOG.error("Stream close exception");
            }
        }
        return flag;
    }
    public Jewel deserialization(String fileName) throws InvalidObjectException {
        File fr = new File(fileName);
        ObjectInputStream istream = null;
        try {
            FileInputStream fis = new FileInputStream(fr);
            istream = new ObjectInputStream(fis);
            Jewel jewel = (Jewel) istream.readObject();
            return jewel;
        } catch (ClassNotFoundException ce) {
            LOG.error("Class doesn't exists: " + ce);
        } catch (FileNotFoundException e) {
            LOG.error("File doesn't exist: "+ e);
        } catch (InvalidClassException ioe) {
            LOG.error("Classes have different versions: " + ioe);
        } catch (IOException ioe) {
            LOG.error("Standart I/O eror: " + ioe);
        } finally {
            try {
                if (istream != null) {
                    istream.close();
                }
            } catch (IOException e) {
                LOG.error("Stream close exception");
            }
        }
        throw new InvalidObjectException("Object hasn't been deserialized");
    }
}