package ru.botyanov.jewel.exception;

public class JewelNotFoundException extends Exception {
    public JewelNotFoundException() {
        super();
    }

    public JewelNotFoundException(String message) {
        super(message);
    }

    public JewelNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public JewelNotFoundException(Throwable cause) {
        super(cause);
    }

    protected JewelNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
