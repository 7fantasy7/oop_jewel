package ru.botyanov.jewel.exception;

public class JewelIllegalArgumentException extends Exception {
    public JewelIllegalArgumentException() {
    }

    public JewelIllegalArgumentException(String message) {
        super(message);
    }

    public JewelIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public JewelIllegalArgumentException(Throwable cause) {
        super(cause);
    }

    public JewelIllegalArgumentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
