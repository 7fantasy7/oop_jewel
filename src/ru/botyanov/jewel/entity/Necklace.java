package ru.botyanov.jewel.entity;

import ru.botyanov.jewel.entity.jewel.Jewel;

import java.util.ArrayList;
import java.util.Collection;

public class Necklace extends ArrayList<Jewel> {
    private static final int JEWEL_COUNT = 30;
    public Necklace(Collection<? extends Jewel> c) {
        super(c);
    }

    public Necklace() {
    }

    @Override
    public boolean add(Jewel jewel) {
        if(size() < JEWEL_COUNT ) {
            add(jewel);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Necklace{\n");
        for(Jewel jewel:this){
            sb.append(jewel).append('\n');
        }
        sb.append('}');
        return sb.toString();
    }
}