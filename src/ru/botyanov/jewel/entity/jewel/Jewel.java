package ru.botyanov.jewel.entity.jewel;

import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;
import ru.botyanov.jewel.exception.JewelIllegalArgumentException;

import java.io.Serializable;

public abstract class Jewel implements Serializable {
    private String name;
    private int price;
    private int weight;
    private int hardness;
    private double transparency;
    private Color color;
    private Cut cut;
    private static final long serialVersionUID = 1L;


    public Jewel(String name, int price, int weight, double transparency, Color color, Cut cut, int hardness) {
        this.name = name;
        this.price = price;
        this.weight = weight;
        this.transparency = transparency;
        this.color = color;
        this.cut = cut;
        this.hardness = hardness;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws JewelIllegalArgumentException {
        if (name != null && !name.isEmpty()) {
            this.name = name;
        } else {
            throw new JewelIllegalArgumentException();
        }
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) throws JewelIllegalArgumentException {
        if (price > 0) {
            this.price = price;
        } else {
            throw new JewelIllegalArgumentException();
        }
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) throws JewelIllegalArgumentException {
        if (weight > 0) {
            this.weight = weight;
        } else {
            throw new JewelIllegalArgumentException();
        }
    }

    public double getTransparency() {
        return transparency;
    }

    public void setTransparency(double transparency) throws JewelIllegalArgumentException {
        if (transparency > 0 && transparency < 1) {
            this.transparency = transparency;
        } else {
            throw new JewelIllegalArgumentException();
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Cut getCut() {
        return cut;
    }

    public void setCut(Cut cut) {
        this.cut = cut;
    }

    public int getHardness() {
        return hardness;
    }

    public void setHardness(int hardness) throws JewelIllegalArgumentException {
        if (hardness > 0) {
            this.hardness = hardness;
        } else {
            throw new JewelIllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                ", hardness=" + hardness +
                ", transparency=" + transparency +
                ", color=" + color +
                ", cut=" + cut;
    }
}