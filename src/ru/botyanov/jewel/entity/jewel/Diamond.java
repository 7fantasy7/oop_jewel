package ru.botyanov.jewel.entity.jewel;

import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;
import ru.botyanov.jewel.enums.Nativity;

public class Diamond extends Jewel {
    private static final int HARDNESS = 10;

    public enum Origin {BOTSWANA, RUSSIA, CANADA, RSA, ANGOLA, OTHER}

    private Origin origin;
    private Nativity nativity;

    public Diamond(String name, int price, int weight, double transparency, Color color,
                   Cut cut, Nativity nativity, Origin origin) {
        super(name, price, weight, transparency, color, cut, HARDNESS);
        this.nativity = nativity;
        this.origin = origin;
    }

    public Nativity getNativity() {
        return nativity;
    }

    public void setNativity(Nativity nativity) {
        this.nativity = nativity;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    @Override
    public String toString() {
        return "Diamond{" + super.toString() +
                " nativity=" + nativity +
                ", origin=" + origin +
                '}';
    }
}
