package ru.botyanov.jewel.entity.jewel;

import org.apache.log4j.Logger;
import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;
import ru.botyanov.jewel.enums.Nativity;
import ru.botyanov.jewel.exception.JewelIllegalArgumentException;

public class Ruby extends Jewel {
    private static final int HARDNESS = 9;
    public static final Logger LOG = Logger.getLogger(Ruby.class);
    private int quality;
    private Nativity nativity;

    public Ruby(String name, int price, int weight, double transparency,
                Color color, Cut cut, Nativity nativity, int quality) {
        super(name, price, weight, transparency, color, cut, HARDNESS);
        this.nativity = nativity;
        this.quality = quality;
    }

    public Nativity getNativity() {
        return nativity;
    }

    public void setNativity(Nativity nativity) {
        this.nativity = nativity;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) throws JewelIllegalArgumentException {
        if (quality > 0 && quality < 4) {
            this.quality = quality;
        } else {
            JewelIllegalArgumentException e = new JewelIllegalArgumentException();
            LOG.error("Quality value isn't valid ", e);
            throw e;
        }
    }

    @Override
    public String toString() {
        return "Ruby{" + super.toString() +
                " quality=" + quality +
                ", nativity=" + nativity +
                '}';
    }
}
