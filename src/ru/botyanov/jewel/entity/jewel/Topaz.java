package ru.botyanov.jewel.entity.jewel;

import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;

public class Topaz extends Jewel {
    private static final int HARDNESS = 8;

    public enum Impurity {Fe, Ti, Cr, V}

    private Impurity impurity;

    public Topaz(String name, int price, int weight, double transparency,
                 Color color, Cut cut, Impurity impurity) {
        super(name, price, weight, transparency, color, cut, HARDNESS);
        this.impurity = impurity;
    }

    public Impurity getImpurity() {
        return impurity;
    }

    public void setImpurity(Impurity impurity) {
        this.impurity = impurity;
    }

    @Override
    public String toString() {
        return "Topaz{" + super.toString() +
                " impurity=" + impurity +
                '}';
    }
}
