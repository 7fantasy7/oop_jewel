package ru.botyanov.jewel.entity.jewel;

import org.apache.log4j.Logger;
import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;
import ru.botyanov.jewel.enums.Nativity;
import ru.botyanov.jewel.exception.JewelIllegalArgumentException;

public class Pearl extends Jewel {
    private static final int HARDNESS = 4;
    public static final Logger LOG = Logger.getLogger(Pearl.class);

    private Nativity nativity;
    private double radiance;
    private double surfaceCleanliness;

    public Pearl(String name, int price, int weight, double transparency, Color color, Cut cut,
                 int hardness, Nativity nativity, double radiance, double surfaceCleanliness) {
        super(name, price, weight, transparency, color, cut, hardness);
        this.nativity = nativity;
        this.radiance = radiance;
        this.surfaceCleanliness = surfaceCleanliness;
    }

    public Nativity getNativity() {
        return nativity;
    }

    public void setNativity(Nativity nativity) {
        this.nativity = nativity;
    }

    public double getRadiance() {
        return radiance;
    }

    public void setRadiance(double radiance) throws JewelIllegalArgumentException {
        if (radiance > 0 && radiance < 100) {
            this.radiance = radiance;
        } else {
            JewelIllegalArgumentException e = new JewelIllegalArgumentException();
            LOG.error("Radiance value isn't valid ", e);
            throw e;
        }
    }

    public double getSurfaceCleanliness() {
        return surfaceCleanliness;
    }

    public void setSurfaceCleanliness(double surfaceCleanliness) throws JewelIllegalArgumentException {
        if (surfaceCleanliness > 0 && surfaceCleanliness < 100) {
            this.surfaceCleanliness = surfaceCleanliness;
        } else {
            JewelIllegalArgumentException e = new JewelIllegalArgumentException();
            LOG.error("SurfaceCleanliness value isn't valid ", e);
            throw e;
        }
    }

    @Override
    public String toString() {
        return "Pearl{" + super.toString() +
                " nativity=" + nativity +
                ", radiance=" + radiance +
                ", surfaceCleanliness=" + surfaceCleanliness +
                '}';
    }
}
