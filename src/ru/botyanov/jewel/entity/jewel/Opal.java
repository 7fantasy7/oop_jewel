package ru.botyanov.jewel.entity.jewel;

import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;

public class Opal extends Jewel {
    private static final int HARDNESS = 6;
    public enum Type { FIRE, BLACK, BOULDER, PERIDOT }
    public enum Impurity { CaO, MgO, Al2O3, Fe2O3, FeO, Na2O, K2O }
    private Type type;
    private Impurity impurity;
    private boolean noblity;

    public Opal(String name, int price, int weight, double transparency, Color color, Cut cut,
                Type type, Impurity impurity, boolean noblity) {
        super(name, price, weight, transparency, color, cut, HARDNESS);
        this.type = type;
        this.impurity = impurity;
        this.noblity = noblity;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Impurity getImpurity() {
        return impurity;
    }

    public void setImpurity(Impurity impurity) {
        this.impurity = impurity;
    }

    public boolean isNoblity() {
        return noblity;
    }

    public void setNoblity(boolean noblity) {
        this.noblity = noblity;
    }

    @Override
    public String toString() {
        return "Opal{" + super.toString() +
                " type=" + type +
                ", impurity=" + impurity +
                ", noblity=" + noblity +
                '}';
    }
}