package ru.botyanov.jewel.main;

import javafx.util.Pair;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import ru.botyanov.jewel.action.NecklaceAction;
import ru.botyanov.jewel.creator.JewelCreator;
import ru.botyanov.jewel.entity.Necklace;
import ru.botyanov.jewel.entity.jewel.Jewel;
import ru.botyanov.jewel.entity.jewel.Opal;
import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;
import ru.botyanov.jewel.serial.Serializator;

import java.io.InvalidObjectException;
import java.util.ArrayList;

public class Runner {
    private static final String LOG_PATH = "config/log4j.xml";

    static {
        new DOMConfigurator().doConfigure(LOG_PATH, LogManager.getLoggerRepository());
    }

    public static final Logger LOG = Logger.getLogger(Runner.class);

    public static void main(String[] args) {
        ArrayList<Jewel> jewelList = JewelCreator.createListJewels();
        Necklace necklace = new Necklace(jewelList);
        Pair<Integer, Integer> result = NecklaceAction.calculateWeightAndPrice(necklace);
        LOG.debug("Full weight: " + result.getKey() + ", price: " + result.getValue());
        NecklaceAction.sort(necklace);
        LOG.debug(necklace);
        LOG.debug(NecklaceAction.findJewelsByTransparency(necklace, 0.1, 0.4).toString());
        LOG.info("Java 8 methods:");
        LOG.info(necklace);
        NecklaceAction.sortJewelsByTransparencyThenPrice(necklace);
        LOG.info("Sorted by transparency & price then\n" + necklace);
        try {
            LOG.info("Nearest to transparency 0.5:\n" + NecklaceAction.findNearestJewelToTransparency(necklace, 0.5));
        } catch (Throwable throwable) {
            LOG.error(throwable + ": in findNearestJewelToTransparency()");
        }
        LOG.info("Jewels cheaper then 70:\n" + NecklaceAction.findJewelsCheaperThen(necklace, 70));
        LOG.info("Full necklace weight: " + NecklaceAction.streamCalculateWeight(necklace));



        Jewel jewel = new Opal("Opal", 52, 4, 0.69, Color.YELLOW, Cut.EMERALD, Opal.Type.FIRE, Opal.Impurity.FeO, false);
        System.out.println(jewel);
        String file = "data/jewel.data";
        Serializator sz = new Serializator();
        boolean b = sz.serialization(jewel, file);

        Jewel res = null;
        try {
            res = sz.deserialization(file);
        } catch (InvalidObjectException e) {
            LOG.error("Invalid object " + e);
        }
        LOG.info("Deserialized object\n" + res);
    }
}