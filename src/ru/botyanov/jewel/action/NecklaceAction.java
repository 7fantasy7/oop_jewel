package ru.botyanov.jewel.action;

import javafx.util.Pair;
import ru.botyanov.jewel.comparator.JewelComparator;
import ru.botyanov.jewel.entity.Necklace;
import ru.botyanov.jewel.entity.jewel.Jewel;
import ru.botyanov.jewel.exception.JewelNotFoundException;

import java.util.*;

public class NecklaceAction {
    public static void sort(Necklace necklace) {
        Collections.sort(necklace, new JewelComparator());
    }

    public static List<Jewel> sortJewelsByTransparency(Necklace necklace) {
        necklace.sort(Comparator.comparing(Jewel::getTransparency));//Comparator
        return necklace;
    }

    public static List<Jewel> sortJewelsByTransparencyThenPrice(Necklace necklace) {
        necklace.sort(Comparator.comparing(Jewel::getTransparency).thenComparing(Jewel::getPrice));//Comparator
        return necklace;
    }

    public static Jewel findNearestJewelToTransparency(Necklace necklace, double transparency) throws Throwable {
        Optional foundJewel = necklace.stream().sorted((o1, o2) -> {
            return (Math.abs(o1.getTransparency() - transparency) > Math.abs(o2.getTransparency() - transparency)) ? 1 : -1;
        })
                .findFirst();//Optional
        return (Jewel) foundJewel.orElseThrow(JewelNotFoundException::new);//Supplier
    }

    public static ArrayList<Jewel> findJewelsCheaperThen(Necklace necklace, int price) {
        ArrayList<Jewel> found = new ArrayList<>();
        necklace.stream().filter((o) -> o.getPrice() < price).forEach(found::add);//Predicate, Consumer
        return found;
    }

    public static int streamCalculateWeight(Necklace necklace) {
        return necklace.stream().mapToInt(Jewel::getWeight).//Function
                sum();
    }

    public static Pair<Integer, Integer> streamCalculateWeightAndPrice(Necklace necklace) {
        return new Pair<>(necklace.stream().mapToInt(Jewel::getWeight).//Function
                sum(), necklace.stream().mapToInt(Jewel::getPrice).//Function
                sum());
    }


    public static ArrayList<Jewel> findJewelsByTransparency(Necklace necklace, double from, double to) {
        ArrayList<Jewel> found = new ArrayList<>();
        for (Jewel jewel : necklace) {
            if (jewel.getTransparency() >= from && jewel.getTransparency() <= to) {
                found.add(jewel);
            }
        }
        return found;
    }

    public static Pair<Integer, Integer> calculateWeightAndPrice(Necklace necklace) {
        int weight = 0;
        int price = 0;
        for (Jewel jewel : necklace) {
            weight += jewel.getWeight();
            price += jewel.getPrice();
        }
        return new Pair<Integer, Integer>(weight, price);
    }

    public static int calculateWeight(Necklace necklace) {
        int weight = 0;
        for (Jewel jewel : necklace) {
            weight += jewel.getWeight();
        }
        return weight;
    }

    public static int calculatePrice(Necklace necklace) {
        int price = 0;
        for (Jewel jewel : necklace) {
            price += jewel.getPrice();
        }
        return price;
    }

}