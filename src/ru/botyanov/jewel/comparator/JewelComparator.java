package ru.botyanov.jewel.comparator;

import ru.botyanov.jewel.entity.jewel.Jewel;

import java.util.Comparator;

public class JewelComparator implements Comparator<Jewel> {
    @Override
    public int compare(Jewel o1, Jewel o2) {
        return Integer.compare(o1.getPrice(), o2.getPrice());
    }
}
