package ru.botyanov.jewel.creator;


import ru.botyanov.jewel.entity.jewel.*;
import ru.botyanov.jewel.entity.jewel.Diamond.Origin;
import ru.botyanov.jewel.entity.jewel.Opal.Type;
import ru.botyanov.jewel.enums.Color;
import ru.botyanov.jewel.enums.Cut;
import ru.botyanov.jewel.enums.Nativity;

import java.util.ArrayList;

public class JewelCreator {
    public static ArrayList<Jewel> createListJewels() {
        ArrayList<Jewel> list = new ArrayList<>();
        list.add(new Diamond("Diamond", 100, 12, 0.24, Color.WHITE, Cut.BRILLIANT, Nativity.NATIVE, Origin.BOTSWANA));
        list.add(new Opal("Opal", 64, 7, 0.78, Color.RED, Cut.HEART, Type.FIRE, Opal.Impurity.CaO, false));
        list.add(new Opal("Opal", 25, 8, 0.24, Color.BLUE, Cut.PRINCESS, Type.PERIDOT, Opal.Impurity.Al2O3, false));
        list.add(new Pearl("Pearl", 72, 15, 0.42, Color.BLUE, Cut.OBUS, 50, Nativity.SYNTHETIC, 45.2, 85.2));
        list.add(new Ruby("Ruby", 112, 21, 0.49, Color.RED, Cut.EMERALD, Nativity.NATIVE, 3));
        list.add(new Topaz("Topaz", 42, 9, 0.92, Color.YELLOW, Cut.OBUS, Topaz.Impurity.Ti));
        list.add(new Topaz("Topaz2", 49, 12, 0.92, Color.CREAM, Cut.ROUND, Topaz.Impurity.Cr));

        return list;
    }
}